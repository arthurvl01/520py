"""
Leia duas notas
Calcule e mostre a media
"""

qtd = int(input('Quantidade de notas: '))

soma = 0

for x in range(qtd):
    nota = float(input('nota{} '.format(x+1)))
    if nota > 10:
        qtd -= 1
        continue
    soma += nota
 
media = soma / qtd

print("Media: {:.2f}".format(media))
